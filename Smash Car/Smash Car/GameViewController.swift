//
//  GameViewController.swift
//  Smash Car
//
//  Created by Allison Lindner on 25/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

enum PhyBodyType: UInt32 {
	case SCENE = 1;
	case PLAYER = 2;
	case MACHINE_GUN_BULLET = 4;
	case WEAPON_TRIGGER = 8;
	case DEFENSE_SAW = 16;
	case DEFENSE_TRIGGER = 32;
}

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

		let scene = GameScene(size: CGSizeMake(2048/2, 1536/2))
		// Configure the view.
		let skView = self.view as! SKView
		//skView.showsFPS = true
		//skView.showsNodeCount = true
		//skView.showsPhysics = true
		
		/* Sprite Kit applies additional optimizations to improve rendering performance */
		skView.ignoresSiblingOrder = true
		
		/* Set the scale mode to scale to fit the window */
		scene.scaleMode = SKSceneScaleMode.Fill
		
		scene.physicsWorld.contactDelegate = scene
		scene.physicsWorld.gravity = CGVectorMake(0, 0)
		
		scene.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.view.frame)
		scene.physicsBody?.categoryBitMask = PhyBodyType.SCENE.rawValue
		scene.physicsBody?.collisionBitMask = PhyBodyType.PLAYER.rawValue
		scene.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
		
		skView.presentScene(scene)
	}

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.AllButUpsideDown
        } else {
            return UIInterfaceOrientationMask.All
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
