//
//  Weapon.swift
//  Smash Car
//
//  Created by Allison Lindner on 26/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class Weapon: NSObject {
	
	var fireRate: Double = 480			//Tiros por minuto
	var timeToShoot: Double = 1			//Tempo para o disparo
	var weaponDuration: Double = 3
	var defenseDuration: Double = 3
	var lastUpdate: CFTimeInterval = 0	//Time do Ultimo Update
	var bulletTexture: SKTexture!
	var bulletSize: CGSize!
	var bullet: SKSpriteNode!
	var defenseTexture: SKTexture!
	var defenseSize: CGSize!
	var defense1: SKSpriteNode!
	var defense2: SKSpriteNode!
	
	func update(player: Player, scene: SKScene, currentTime: CFTimeInterval) {
		if lastUpdate == 0 {
			lastUpdate = currentTime
			timeToShoot = 60/fireRate
		}
		
		let deltaTime = currentTime - lastUpdate
		lastUpdate = currentTime
		
		if player.weaponActivated {
			timeToShoot -= deltaTime
			weaponDuration -= deltaTime
			
			if timeToShoot <= 0 {
				//Shoot
				let bulletCopy = bullet.copy() as! SKSpriteNode
				
				bulletCopy.zRotation = player.zRotation
				bulletCopy.position = player.position
				bulletCopy.position.x += -sin(bulletCopy.zRotation) * 40
				bulletCopy.position.y += cos(bulletCopy.zRotation) * 40
				
				bulletCopy.physicsBody?.velocity = CGVectorMake(-sin(bulletCopy.zRotation) * 1000, cos(bulletCopy.zRotation) * 1000)
				
				scene.addChild(bulletCopy)
				timeToShoot = 60/fireRate
			}
		}
		
		if player.defenseActivated {
			player.weapon.defense1.physicsBody?.angularVelocity = 40
			player.particleFireSaw1!.particlePosition = player.weapon.defense1.position
			
			defenseDuration -= deltaTime
		}
	}
}
