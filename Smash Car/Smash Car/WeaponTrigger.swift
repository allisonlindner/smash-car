//
//  WeaponTrigger.swift
//  Smash Car
//
//  Created by Allison Lindner on 26/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class WeaponTrigger: SKSpriteNode {
	
	var activated = false
	
	func activateOnPlayer(player: Player) {
		player.setupMachineGun()
		player.weaponActivated = true
	}
	
	func activate() {
		self.texture = SKTexture(imageNamed: "gunActivated.png")
		self.activated = true
	}
	
	func deactivate() {
		self.texture = SKTexture(imageNamed: "gunDeactivated.png")
		self.activated = false
	}
}
