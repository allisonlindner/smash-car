//
//  GameScene.swift
//  Smash Car
//
//  Created by Allison Lindner on 25/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let BUTTON_POSITION: CGFloat = 140
    let BUTTON_SIZE = CGSizeMake(140, 140)
    let PLAYER_SIZE = CGSizeMake(40, 60)
    
    var player1: Player!
    var player2: Player!
    var player3: Player!
    var player4: Player!
    
    var weaponTrigger1: WeaponTrigger!
    var weaponTrigger2: WeaponTrigger!
    
    var defenseTrigger1: DefenseTrigger!
    var defenseTrigger2: DefenseTrigger!
    
    var ding:AVAudioPlayer = AVAudioPlayer()
    var countDown = SKLabelNode(text: "3")
    
    var lastUpdate: CFTimeInterval = 0
    var lastUpdateHold: CFTimeInterval = 0
    var activationWeaponTriggerTimer: CFTimeInterval = 10
    var activationDefenseTriggerTimer: CFTimeInterval = 10
    var initialHold: CFTimeInterval = 3
    var deltaTimeHold:CFTimeInterval = 0
    
    var numberOfPlayers = SKLabelNode(text: "2 Players")
    let BUTTONMENU_SIZE = CGSizeMake(100, 100)
    var onMenu:Bool = true
    
    override func didMoveToView(view: SKView) {
        prepareMenu()
    }
    
    //MARK: Menu
    func prepareMenu(){
        self.removeAllChildren()
        
        self.backgroundColor = UIColor(red: 66/255.0, green: 66/255.0, blue: 66/255.0, alpha: 1)
        let leftButton = SKSpriteNode (imageNamed: "Player1_left.png")
        leftButton.name = "left"
        leftButton.size = BUTTONMENU_SIZE
        leftButton.position = CGPointMake(self.size.width/2 - 300, self.size.height/2 - 200)
        leftButton.zRotation = CGFloat(M_PI/4.0)
        
        let rightButton = SKSpriteNode (imageNamed: "Player1_right.png")
        rightButton.name = "right"
        rightButton.size = BUTTON_SIZE
        rightButton.position = CGPointMake(self.size.width/2 + 300, self.size.height/2 - 200)
        rightButton.zRotation = CGFloat(M_PI/4.0)
        
        let startButton = SKLabelNode(text: "Start")
        startButton.name = "start"
        startButton.fontSize = 150
        startButton.position = CGPointMake(self.size.width/2 , self.size.height/2)
        startButton.fontName = "Superclarendon"
        
        numberOfPlayers.fontSize = 130
        numberOfPlayers.position = CGPointMake(self.size.width/2 , self.size.height/2 - 240)
        
        self.addChild(numberOfPlayers)
        self.addChild(startButton)
        
        self.addChild(leftButton)
        self.addChild(rightButton)
        
    }
    
    //MARK: GameScene
    func prepareGameScene(){
        self.removeAllChildren()
        self.backgroundColor = UIColor(red: 66/255.0, green: 66/255.0, blue: 66/255.0, alpha: 1)
        let arena1 = SKSpriteNode(texture: SKTexture(imageNamed: "arena1.png"), size: CGSizeMake(351, 351))
        arena1.position.x = 0 + arena1.size.width/2
        arena1.position.y = 0 + arena1.size.height/2
        arena1.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "arena1.png"), size: CGSizeMake(351, 351))
        arena1.physicsBody?.categoryBitMask = PhyBodyType.SCENE.rawValue
        arena1.physicsBody?.collisionBitMask = PhyBodyType.PLAYER.rawValue
        arena1.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        arena1.physicsBody?.dynamic = false
        
        let arena2 = SKSpriteNode(texture: SKTexture(imageNamed: "arena2.png"), size: CGSizeMake(351, 351))
        arena2.position.x = self.size.width - arena2.size.width/2
        arena2.position.y = 0 + arena2.size.height/2
        arena2.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "arena2.png"), size: CGSizeMake(351, 351))
        arena2.physicsBody?.categoryBitMask = PhyBodyType.SCENE.rawValue
        arena2.physicsBody?.collisionBitMask = PhyBodyType.PLAYER.rawValue
        arena2.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        arena2.physicsBody?.dynamic = false
        
        let arena3 = SKSpriteNode(texture: SKTexture(imageNamed: "arena3.png"), size: CGSizeMake(351, 351))
        arena3.position.x = self.size.width - arena3.size.width/2
        arena3.position.y = self.size.height - arena3.size.height/2
        arena3.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "arena3.png"), size: CGSizeMake(351, 351))
        arena3.physicsBody?.categoryBitMask = PhyBodyType.SCENE.rawValue
        arena3.physicsBody?.collisionBitMask = PhyBodyType.PLAYER.rawValue
        arena3.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        arena3.physicsBody?.dynamic = false
        
        let arena4 = SKSpriteNode(texture: SKTexture(imageNamed: "arena4.png"), size: CGSizeMake(351, 351))
        arena4.position.x = 0 + arena4.size.width/2
        arena4.position.y = self.size.height - arena4.size.height/2
        arena4.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "arena4.png"), size: CGSizeMake(351, 351))
        arena4.physicsBody?.categoryBitMask = PhyBodyType.SCENE.rawValue
        arena4.physicsBody?.collisionBitMask = PhyBodyType.PLAYER.rawValue
        arena4.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        arena4.physicsBody?.dynamic = false
        
        self.addChild(arena1)
        self.addChild(arena2)
        self.addChild(arena3)
        self.addChild(arena4)
        
        //MARK: WEAPON TRIGGER
        weaponTrigger1 = WeaponTrigger(color: UIColor.blackColor(), size: CGSizeMake(50, 50))
        weaponTrigger1.position = CGPointMake(self.size.width/2, self.size.height - 150)
        weaponTrigger1.deactivate()
        weaponTrigger1.physicsBody = SKPhysicsBody(circleOfRadius: weaponTrigger1.size.width/2)
        weaponTrigger1.physicsBody?.categoryBitMask = PhyBodyType.WEAPON_TRIGGER.rawValue
        weaponTrigger1.physicsBody?.collisionBitMask = 0
        weaponTrigger1.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        
        weaponTrigger2 = WeaponTrigger(color: UIColor.blackColor(), size: CGSizeMake(50, 50))
        weaponTrigger2.position = CGPointMake(self.size.width/2, 150)
        weaponTrigger2.deactivate()
        weaponTrigger2.physicsBody = SKPhysicsBody(circleOfRadius: weaponTrigger2.size.width/2)
        weaponTrigger2.physicsBody?.categoryBitMask = PhyBodyType.WEAPON_TRIGGER.rawValue
        weaponTrigger2.physicsBody?.collisionBitMask = 0
        weaponTrigger2.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        
        //MARK: WEAPON TRIGGER
        defenseTrigger1 = DefenseTrigger(color: UIColor.blackColor(), size: CGSizeMake(50, 50))
        defenseTrigger1.position = CGPointMake(150, self.size.height/2)
        defenseTrigger1.deactivate()
        defenseTrigger1.physicsBody = SKPhysicsBody(circleOfRadius: defenseTrigger1.size.width/2)
        defenseTrigger1.physicsBody?.categoryBitMask = PhyBodyType.DEFENSE_TRIGGER.rawValue
        defenseTrigger1.physicsBody?.collisionBitMask = 0
        defenseTrigger1.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        
        defenseTrigger2 = DefenseTrigger(color: UIColor.blackColor(), size: CGSizeMake(50, 50))
        defenseTrigger2.position = CGPointMake(self.size.width - 150, self.size.height/2)
        defenseTrigger2.deactivate()
        defenseTrigger2.physicsBody = SKPhysicsBody(circleOfRadius: defenseTrigger2.size.width/2)
        defenseTrigger2.physicsBody?.categoryBitMask = PhyBodyType.DEFENSE_TRIGGER.rawValue
        defenseTrigger2.physicsBody?.collisionBitMask = 0
        defenseTrigger2.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
        
        // Weapon Triggers
        self.addChild(weaponTrigger1)
        self.addChild(weaponTrigger2)
        
        // Defense Triggers
        self.addChild(defenseTrigger1)
        self.addChild(defenseTrigger2)
        
        
        
        setupP1()
        setupP2()
        if numberOfPlayers.text == "3 Players" || numberOfPlayers.text == "4 Players" {
            setupP3()
        }
        if numberOfPlayers.text == "4 Players" {
            setupP4()
        }
        
        //Setup countdown label
        countDown.fontSize = 130
        countDown.position = CGPointMake(self.size.width/2, self.size.height/2)
        countDown.name = "countdown"
        countDown.text = "3"
        self.addChild(countDown)
        
        prepareAudios("fundo")
        ding.play()
        
        

    }
    

    
    func prepareAudios(musica : String) {
        
        let path = NSBundle.mainBundle().pathForResource(musica, ofType: "mp3")
        do {
            ding = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: path!))
        } catch _ {
            
        }
        ding.prepareToPlay()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            let node = self.nodeAtPoint(location)
            
            //menu part
            if node.name == "left" {
                if numberOfPlayers.text == "2 Players"{
                    numberOfPlayers.text = "4 Players"
                }
                else if numberOfPlayers.text == "3 Players"{
                    numberOfPlayers.text = "2 Players"
                }
                    
                else if numberOfPlayers.text == "4 Players"{
                    numberOfPlayers.text = "3 Players"
                }
            }
            if node.name == "right" {
                if numberOfPlayers.text == "2 Players"{
                    numberOfPlayers.text = "3 Players"
                }
                else if numberOfPlayers.text == "3 Players"{
                    numberOfPlayers.text = "4 Players"
                }
                    
                else if numberOfPlayers.text == "4 Players"{
                    numberOfPlayers.text = "2 Players"
                }
            }
            if node.name == "start" {
                prepareGameScene()
                onMenu = false
                lastUpdateHold = 0
            }
            
            if node.name == "aux"{
                prepareMenu()
                onMenu = true
                ding.stop()
                deltaTimeHold = 0
            }
            
            //update part
            if node.name == "player1LeftArrow" {
                player1.left = true
            }
            if node.name == "player1RightArrow" {
                player1.right = true
            }
            
            if node.name == "player2LeftArrow" {
                player2.left = true
            }
            if node.name == "player2RightArrow" {
                player2.right = true
            }
            
            if node.name == "player3LeftArrow" {
                player3.left = true
            }
            if node.name == "player3RightArrow" {
                player3.right = true
            }
            
            if node.name == "player4LeftArrow" {
                player4.left = true
            }
            if node.name == "player4RightArrow" {
                player4.right = true
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            
            let node = self.nodeAtPoint(location)
            
            if node.name == "player1LeftArrow" {
                player1.left = false
            }
            if node.name == "player1RightArrow" {
                player1.right = false
            }
            
            if node.name == "player2LeftArrow" {
                player2.left = false
            }
            if node.name == "player2RightArrow" {
                player2.right = false
            }
            
            if node.name == "player3LeftArrow" {
                player3.left = false
            }
            if node.name == "player3RightArrow" {
                player3.right = false
            }
            
            if node.name == "player4LeftArrow" {
                player4.left = false
            }
            if node.name == "player4RightArrow" {
                player4.right = false
            }
        }
    }
    

    override func update(currentTime: CFTimeInterval) {
        
        
        if !onMenu {
            if lastUpdateHold == 0 {
                lastUpdateHold = currentTime
            }
            
            
            deltaTimeHold = currentTime - lastUpdateHold
            
            if deltaTimeHold > initialHold {
                
                if lastUpdate == 0 {
                    lastUpdate = currentTime
                }
                
                let deltaTime = currentTime - lastUpdate
                //MARK: Activation of Weapon Triggers
                if weaponTrigger1.activated || weaponTrigger2.activated {
                    activationWeaponTriggerTimer = 10
                } else {
                    activationWeaponTriggerTimer -= deltaTime
                }
                
                if activationWeaponTriggerTimer <= 0 {
                    switch(arc4random()%2) {
                    case 0:
                        weaponTrigger1.activate()
                        break
                    case 1:
                        weaponTrigger2.activate()
                        break
                    default:
                        break
                    }
                    activationWeaponTriggerTimer = 10
                }
                
                //MARK: Activation of Shield Triggers
                if defenseTrigger1.activated || defenseTrigger2.activated {
                    activationDefenseTriggerTimer = 10
                } else {
                    activationDefenseTriggerTimer -= deltaTime
                }
                
                if activationDefenseTriggerTimer <= 0 {
                    switch(arc4random()%2) {
                    case 0:
                        defenseTrigger1.activate()
                        break
                    case 1:
                        defenseTrigger2.activate()
                        break
                    default:
                        break
                    }
                    activationDefenseTriggerTimer = 10
                }
                
                //MARK: Update Players
                player1.Update(self, currentTime: currentTime, deltaTime: deltaTime)
                player2.Update(self, currentTime: currentTime, deltaTime: deltaTime)
                if numberOfPlayers.text == "3 Players" || numberOfPlayers.text == "4 Players" {
                    player3.Update(self, currentTime: currentTime, deltaTime: deltaTime)
                }
                if numberOfPlayers.text == "4 Players" {
                    player4.Update(self, currentTime: currentTime, deltaTime: deltaTime)
                }
                
                lastUpdate = currentTime
                
                if numberOfPlayers.text == "2 Players" {
                    if player2.HP <= 0 {
                        
                        self.countDown.text = "Blue Player Wins!"
                        
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                    }
                    else if player1.HP <= 0 {
                        
                        self.countDown.text = "Green Player Wins!"
                        
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                    }
                }
                else if numberOfPlayers.text == "3 Players" {
                    if player2.HP <= 0 && player3.HP <= 0{
                        
                        self.countDown.text = "Blue Player Wins!"
                        
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                        player3.carVelocity = 0
                        
                    }
                    else if player1.HP <= 0 && player3.HP <= 0{
                        
                        self.countDown.text = "Green Player Wins!"
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                        player3.carVelocity = 0
                        
                    }
                        
                    else if player1.HP <= 0 && player2.HP <= 0 {
                        
                        self.countDown.text = "Red Player Wins!"
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                        player3.carVelocity = 0
                        
                    }
                }
                else if numberOfPlayers.text == "4 Players" {
                    if player2.HP <= 0 && player3.HP <= 0 && player4.HP <= 0{
                        
                        self.countDown.text = "Blue Player Wins!"
                        
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                        player3.carVelocity = 0
                        player4.carVelocity = 0
                        
                    }
                    else if player1.HP <= 0 && player3.HP <= 0 && player4.HP <= 0 {
                        
                        self.countDown.text = "Green Player Wins!"
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                        player3.carVelocity = 0
                        player4.carVelocity = 0
                        
                    }
                        
                    else if player1.HP <= 0 && player2.HP <= 0 && player4.HP <= 0 {
                        
                        self.countDown.text = "Red Player Wins!"
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                        player3.carVelocity = 0
                        player4.carVelocity = 0
                        
                    }
                        
                    else if player1.HP <= 0 && player2.HP <= 0 && player3.HP <= 0 {
                        
                        self.countDown.text = "Purple Player Wins!"
                        self.countDown.removeFromParent()
                        self.addChild(countDown)
                        
                        player1.carVelocity = 0
                        player2.carVelocity = 0
                        player3.carVelocity = 0
                        player4.carVelocity = 0
                        
                    }
                }
                
            } else {
                if deltaTimeHold < 1 {
                    self.countDown.text = "3"
                }
                else if deltaTimeHold <= 2 {
                    self.countDown.text = "2"
                }
                else if deltaTimeHold <= 2.9 {
                    self.countDown.text = "1"
                }
                else if deltaTimeHold <= 3 {
                    self.countDown.removeFromParent()
                    deltaTimeHold = 3
                    countDown.name = "aux"
                }
                

                    

                
                
            }
        }
        
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch (contactMask) {
            
        case PhyBodyType.MACHINE_GUN_BULLET.rawValue | PhyBodyType.PLAYER.rawValue:
            
            if contact.bodyA.categoryBitMask == PhyBodyType.MACHINE_GUN_BULLET.rawValue {
                contact.bodyA.node?.removeFromParent()
                
                if let player = contact.bodyB.node as? Player {
                    player.HP -= 1
                }
            } else {
                contact.bodyB.node?.removeFromParent()
                
                if let player = contact.bodyA.node as? Player {
                    player.HP -= 1
                }
            }
            
            break
            
        case PhyBodyType.DEFENSE_SAW.rawValue | PhyBodyType.PLAYER.rawValue:
            
            if contact.bodyA.categoryBitMask == PhyBodyType.DEFENSE_SAW.rawValue {
                let player = contact.bodyB.node as! Player
                if player.timeNextSawCollision <= 0 {
                    player.HP -= 2
                    player.timeNextSawCollision = 0.5
                }
            } else {
                let player = contact.bodyA.node as! Player
                if player.timeNextSawCollision <= 0 {
                    player.HP -= 2
                    player.timeNextSawCollision = 0.5
                }
            }
            
            break
            
        case PhyBodyType.MACHINE_GUN_BULLET.rawValue | PhyBodyType.SCENE.rawValue:
            
            if contact.bodyA.categoryBitMask == PhyBodyType.MACHINE_GUN_BULLET.rawValue {
                contact.bodyA.node?.removeFromParent()
            } else {
                contact.bodyB.node?.removeFromParent()
            }
            
            break
            
        case PhyBodyType.WEAPON_TRIGGER.rawValue | PhyBodyType.PLAYER.rawValue:
            
            if contact.bodyA.categoryBitMask == PhyBodyType.WEAPON_TRIGGER.rawValue {
                let trigger = contact.bodyA.node as! WeaponTrigger
                
                if trigger.activated {
                    trigger.activateOnPlayer(contact.bodyB.node as! Player)
                    trigger.color = UIColor.blackColor()
                    trigger.deactivate()
                }
            } else {
                let trigger = contact.bodyB.node as! WeaponTrigger
                
                if trigger.activated {
                    trigger.activateOnPlayer(contact.bodyA.node as! Player)
                    trigger.color = UIColor.blackColor()
                    trigger.deactivate()
                }
            }
            
            break
            
        case PhyBodyType.DEFENSE_TRIGGER.rawValue | PhyBodyType.PLAYER.rawValue:
            
            if contact.bodyA.categoryBitMask == PhyBodyType.DEFENSE_TRIGGER.rawValue {
                let trigger = contact.bodyA.node as! DefenseTrigger
                
                if trigger.activated {
                    trigger.activateOnPlayer(self, player: contact.bodyB.node as! Player)
                    trigger.deactivate()
                }
            } else {
                let trigger = contact.bodyB.node as! DefenseTrigger
                
                if trigger.activated {
                    trigger.activateOnPlayer(self, player: contact.bodyA.node as! Player)
                    trigger.deactivate()
                }
            }
            
            break
            
        case PhyBodyType.PLAYER.rawValue | PhyBodyType.PLAYER.rawValue:
            
            let player1Collision = contact.bodyA.node as! Player
            let player2Collision = contact.bodyB.node as! Player
            
            let collisionSide1 = verifyCollisionSide(player1Collision, playerCollided2: player2Collision)
            let collisionSide2 = verifyCollisionSide(player2Collision, playerCollided2: player1Collision)
            
            if (collisionSide1 == 1) {
                player1Collision.HP -= 2
            } else if (collisionSide1 == 2) {
                player1Collision.HP -= 4
            } else if (collisionSide1 == 3) {
                player1Collision.HP -= 5
            } else {
                //println("Fora do tempo")
            }
            
            if (collisionSide2 == 1) {
                player2Collision.HP -= 5
            } else if (collisionSide2 == 2) {
                player2Collision.HP -= 2
            } else if (collisionSide2 == 3) {
                player2Collision.HP -= 3
            }
            
            player1Collision.timeNextCollision = 0.3
            player2Collision.timeNextCollision = 0.3
            
            break
            
        default:
            break
        }
    }
    
    //MARK : Collision
    
    func verifyCollisionSide(playerCollided1: Player, playerCollided2: Player) -> Int {
        if playerCollided1.timeNextCollision <= 0 {
            
            let deg1 = playerCollided1.zRotation * CGFloat(180) / CGFloat(M_PI)
            let deg2 = playerCollided2.zRotation * CGFloat(180) / CGFloat(M_PI)
            
            let deg = abs(deg1 - deg2)
            
            if deg >= -200 && deg <= -160 {
                return 3						//Frente
            } else if deg >= -290 && deg <= -250 {
                return 3
            } else if deg >= -110 && deg <= -70 {
                return 3
            } else if deg >= 0 && deg <= 20 {
                if playerCollided1.zRotation > 0 {
                    if playerCollided1.position.x < playerCollided2.position.x {
                        return 2				//Traz
                    } else {
                        return 3				//Frente
                    }
                } else {
                    if playerCollided1.position.x > playerCollided2.position.x {
                        return 2				//Traz
                    } else {
                        return 3				//Frente
                    }
                }
            } else if deg >= 160 && deg <= 200 {
                if playerCollided1.zRotation > 0 {
                    if playerCollided1.position.x < playerCollided2.position.x {
                        return 2				//Traz
                    } else {
                        return 3				//Frente
                    }
                } else {
                    if playerCollided1.position.x > playerCollided2.position.x {
                        return 2				//Traz
                    } else {
                        return 3				//Frente
                    }
                }
            } else {
                return 1
            }
        } else {
            return 0
        }
    }
    
    
    //MARK : Players Setup
    func setupP1(){
        //MARK: PLAYER 01 - Settings
        let player1LeftArrow = SKSpriteNode(imageNamed: "Player1_left.png")
        player1LeftArrow.name = "player1LeftArrow"
        //player1LeftArrow.zRotation = CGFloat(-M_PI/4.0)
        player1LeftArrow.position = CGPointMake(BUTTON_POSITION/2, BUTTON_POSITION)
        player1LeftArrow.zPosition = 2;
        player1LeftArrow.size = BUTTON_SIZE
        
        let player1RightArrow = SKSpriteNode(imageNamed: "Player1_right.png")
        player1RightArrow.name = "player1RightArrow"
        //player1RightArrow.zRotation = CGFloat(-M_PI/4.0)
        player1RightArrow.position = CGPointMake(BUTTON_POSITION, BUTTON_POSITION/2)
        player1RightArrow.zPosition = 2;
        player1RightArrow.size = BUTTON_SIZE
        
        //Setting Car
        player1 = Player(imageNamed: "car1.png")
        player1.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "car1.png"), size: PLAYER_SIZE)
        player1.physicsBody?.dynamic = true
        player1.physicsBody?.angularDamping = 1
        player1.physicsBody?.mass = 1000
        player1.name = "player1"
        player1.position = CGPointMake(BUTTON_POSITION + 60, BUTTON_POSITION + 60)
        player1.size = PLAYER_SIZE
        player1.zRotation = CGFloat(-M_PI/4.0)
        player1.physicsBody?.categoryBitMask = PhyBodyType.PLAYER.rawValue
        player1.physicsBody?.collisionBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player1.physicsBody?.contactTestBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player1.leftDirection = 1
        player1.rightDirection = -1
        
        
        self.addChild(player1.particleSmoke!)
        self.addChild(player1.particleFireSaw2!)
        player1.particleFireSaw2!.hidden = true
        
        //Player 01
        self.addChild(player1LeftArrow)
        self.addChild(player1RightArrow)
        self.addChild(player1)
        
    }
    
    func setupP2(){
        //MARK: PLAYER 02 - Settings
        let player2LeftArrow = SKSpriteNode(imageNamed: "Player2_left.png")
        player2LeftArrow.name = "player2LeftArrow"
        //player2LeftArrow.zRotation = CGFloat(M_PI/4.0)
        player2LeftArrow.position = CGPointMake(self.size.width - BUTTON_POSITION, BUTTON_POSITION/2)
        player2LeftArrow.zPosition = 2;
        player2LeftArrow.size = BUTTON_SIZE
        
        let player2RightArrow = SKSpriteNode(imageNamed: "Player2_right.png")
        player2RightArrow.name = "player2RightArrow"
        //player2RightArrow.zRotation = CGFloat(M_PI/4.0)
        player2RightArrow.position = CGPointMake(self.size.width - (BUTTON_POSITION/2), BUTTON_POSITION)
        player2RightArrow.zPosition = 2;
        player2RightArrow.size = BUTTON_SIZE
        
        //Setting Car
        player2 = Player(imageNamed: "car2.png")
        player2.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "car2.png"), size: PLAYER_SIZE)
        player2.physicsBody?.dynamic = true
        player2.physicsBody?.angularDamping = 1
        player2.physicsBody?.mass = 1000
        player2.name = "player2"
        player2.position = CGPointMake(self.size.width - BUTTON_POSITION - 60, BUTTON_POSITION + 60)
        player2.size = PLAYER_SIZE
        player2.zRotation = CGFloat(M_PI/4.0)
        player2.physicsBody?.categoryBitMask = PhyBodyType.PLAYER.rawValue
        player2.physicsBody?.collisionBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player2.physicsBody?.contactTestBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player2.leftDirection = 1
        player2.rightDirection = -1
        
        
        self.addChild(player2.particleSmoke!)
        self.addChild(player2.particleFireSaw2!)
        player2.particleFireSaw2!.hidden = true
        
        //Player 02
        self.addChild(player2LeftArrow)
        self.addChild(player2RightArrow)
        self.addChild(player2)
        
    }
    
    func setupP3(){
        //MARK: PLAYER 03 - Settings
        let player3LeftArrow = SKSpriteNode(imageNamed: "Player3_left.png")
        player3LeftArrow.name = "player3LeftArrow"
        //player3LeftArrow.zRotation = CGFloat(-M_PI/4.0)
        player3LeftArrow.position = CGPointMake(self.size.width - BUTTON_POSITION, self.size.height - (BUTTON_POSITION/2))
        player3LeftArrow.zPosition = 2;
        player3LeftArrow.size = BUTTON_SIZE
        
        let player3RightArrow = SKSpriteNode(imageNamed: "Player3_right.png")
        player3RightArrow.name = "player3RightArrow"
        //player3RightArrow.zRotation = CGFloat(-M_PI/4.0)
        player3RightArrow.position = CGPointMake(self.size.width - (BUTTON_POSITION/2), self.size.height - BUTTON_POSITION)
        player3RightArrow.zPosition = 2;
        player3RightArrow.size = BUTTON_SIZE
        
        //Setting Car
        player3 = Player(imageNamed: "car3.png")
        player3.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "car3.png"), size: PLAYER_SIZE)
        player3.physicsBody?.dynamic = true
        player3.physicsBody?.angularDamping = 1
        player3.physicsBody?.mass = 1000
        player3.name = "player3"
        player3.position = CGPointMake(self.size.width - BUTTON_POSITION - 60, self.size.height - BUTTON_POSITION - 60)
        player3.size = PLAYER_SIZE
        player3.zRotation = CGFloat(M_PI*3.0/4.0)
        player3.physicsBody?.categoryBitMask = PhyBodyType.PLAYER.rawValue
        player3.physicsBody?.collisionBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player3.physicsBody?.contactTestBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player3.leftDirection = -1
        player3.rightDirection = 1
        
        self.addChild(player3.particleSmoke!)
        self.addChild(player3.particleFireSaw2!)
        player3.particleFireSaw2!.hidden = true
        
        //Player 03
        self.addChild(player3LeftArrow)
        self.addChild(player3RightArrow)
        self.addChild(player3)
    }
    
    func setupP4(){
        //MARK: PLAYER 04 - Settings
        let player4LeftArrow = SKSpriteNode(imageNamed: "Player4_left.png")
        player4LeftArrow.name = "player4LeftArrow"
        //player4LeftArrow.zRotation = CGFloat(M_PI/4.0)
        player4LeftArrow.position = CGPointMake(BUTTON_POSITION/2, self.size.height - BUTTON_POSITION)
        player4LeftArrow.zPosition = 2;
        player4LeftArrow.size = BUTTON_SIZE
        
        let player4RightArrow = SKSpriteNode(imageNamed: "Player4_right.png")
        player4RightArrow.name = "player4RightArrow"
        //player4RightArrow.zRotation = CGFloat(M_PI/4.0)
        player4RightArrow.position = CGPointMake(BUTTON_POSITION, self.size.height - (BUTTON_POSITION/2))
        player4RightArrow.zPosition = 2;
        player4RightArrow.size = BUTTON_SIZE
        
        //Setting Car
        player4 = Player(imageNamed: "car4.png")
        player4.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "car4.png"), size: PLAYER_SIZE)
        player4.physicsBody?.dynamic = true
        player4.physicsBody?.angularDamping = 1
        player4.physicsBody?.mass = 1000
        player4.name = "player4"
        player4.position = CGPointMake(BUTTON_POSITION + 60, self.size.height - BUTTON_POSITION - 60)
        player4.size = PLAYER_SIZE
        player4.zRotation = CGFloat(-M_PI*3.0/4.0)
        player4.physicsBody?.categoryBitMask = PhyBodyType.PLAYER.rawValue
        player4.physicsBody?.collisionBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player4.physicsBody?.contactTestBitMask = PhyBodyType.SCENE.rawValue | PhyBodyType.PLAYER.rawValue
        player4.leftDirection = -1
        player4.rightDirection = 1
        
        self.addChild(player4.particleSmoke!)
        self.addChild(player4.particleFireSaw2!)
        player4.particleFireSaw2!.hidden = true
        
        //Player 04
        self.addChild(player4LeftArrow)
        self.addChild(player4RightArrow)
        self.addChild(player4)
        
        
    }

    
}