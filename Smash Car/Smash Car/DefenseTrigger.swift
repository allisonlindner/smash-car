//
//  DefenseTrigger.swift
//  Smash Car
//
//  Created by Allison Lindner on 27/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class DefenseTrigger: SKSpriteNode {
	
	var activated = false
	
	func activateOnPlayer(scene: SKScene, player: Player) {
		player.setupSaw(scene)
		player.defenseActivated = true
	}
	
	func activate() {
		self.texture = SKTexture(imageNamed: "shieldActivated.png")
		self.activated = true
	}
	
	func deactivate() {
		self.texture = SKTexture(imageNamed: "shieldDeactivated.png")
		self.activated = false
	}
}
