//
//  Player.swift
//  Smash Car
//
//  Created by Allison Lindner on 26/05/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class Player: SKSpriteNode {
	
	var left: Bool = false
	var right: Bool = false
	var leftDirection = -1
	var rightDirection = 1
	var carVelocity: CGFloat = 0
	var carMaxVelocity: CGFloat = 300
	var weaponActivated = false
	var defenseActivated = false
	var weapon = Weapon()
	let particleFireSaw1 = SKEmitterNode(fileNamed: "fireSaw.sks")
	let particleFireSaw2 = SKEmitterNode(fileNamed: "fireExplosion.sks")
	let particleSmoke = SKEmitterNode(fileNamed: "smokeCar.sks")
	var HP = 400
	var timeNextCollision: CFTimeInterval = 1
	var timeNextSawCollision: CFTimeInterval = 0.5
	var dead = false
	
	func setupMachineGun() {
		weapon.bulletTexture = SKTexture(imageNamed: "shoot1.png")
		weapon.bulletSize = CGSize(width: 4, height: 10)
		weapon.weaponDuration = 5
		
		weapon.bullet = SKSpriteNode(texture: weapon.bulletTexture, size: weapon.bulletSize)
		weapon.bullet.physicsBody = SKPhysicsBody(texture: weapon.bulletTexture, size: weapon.bulletSize)
		weapon.bullet.physicsBody?.categoryBitMask = PhyBodyType.MACHINE_GUN_BULLET.rawValue
		weapon.bullet.physicsBody?.collisionBitMask = PhyBodyType.PLAYER.rawValue | PhyBodyType.SCENE.rawValue
		weapon.bullet.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue | PhyBodyType.SCENE.rawValue
		weapon.bullet.physicsBody?.dynamic = true
	}
	
	func setupSaw(scene: SKScene) {
		weapon.defenseTexture = SKTexture(imageNamed: "saw1.png")
		weapon.defenseSize = CGSizeMake(15, 15)
		weapon.defenseDuration = 8
		
		weapon.defense1 = SKSpriteNode(texture: weapon.defenseTexture, size: weapon.defenseSize)
		weapon.defense1.position = self.position
		weapon.defense1.position.x += self.size.width
		weapon.defense1.physicsBody = SKPhysicsBody(circleOfRadius: weapon.defenseSize.width/2)
		weapon.defense1.physicsBody?.categoryBitMask = PhyBodyType.DEFENSE_SAW.rawValue
		weapon.defense1.physicsBody?.collisionBitMask = 0
		weapon.defense1.physicsBody?.contactTestBitMask = PhyBodyType.PLAYER.rawValue
		weapon.defense1.physicsBody?.dynamic = true
		
		weapon.defense1.physicsBody?.angularDamping = 0
		
		scene.addChild(particleFireSaw1!)
		
		scene.addChild(weapon.defense1)
		
		let pin = SKPhysicsJointPin.jointWithBodyA(self.physicsBody!, bodyB: weapon.defense1.physicsBody!, anchor: self.position)
		
		scene.physicsWorld.addJoint(pin)
	}
	
	func Update(scene: SKScene, currentTime: CFTimeInterval, deltaTime: CFTimeInterval) {
		var rotationDirection = 0
		
		if left && right {
			carVelocity -= 50
			if carVelocity < carMaxVelocity * (-1) {
				carVelocity = carMaxVelocity * (-1)
			}
		} else {
			carVelocity += 50
			if carVelocity > carMaxVelocity {
				carVelocity = carMaxVelocity
			}
			
			if left {
				rotationDirection = leftDirection
			} else if right {
				rotationDirection = rightDirection
			}
		}
		
		particleSmoke!.particlePosition = self.position
		
		// VERIFICA O HP PARA DEFINIR A FUMAÇA
		if Double(HP/400) >= 0.9 {
			particleSmoke!.hidden = true
			particleSmoke!.alpha = 0.2
		} else if Double(HP/400) >= 0.7 {
			particleSmoke!.hidden = false
			particleSmoke!.alpha = 0.3
		} else if Double(HP/400) >= 0.4 {
			particleSmoke!.hidden = false
			particleSmoke!.particleBirthRate = 60
			particleSmoke!.alpha = 0.4
		} else if Double(HP/400) >= 0.2 {
			particleSmoke!.hidden = false
			particleSmoke!.particleBirthRate = 80
			particleSmoke!.alpha = 0.6
		} else {
			particleSmoke!.hidden = false
			particleSmoke!.particleBirthRate = 110
			particleSmoke!.alpha = 0.8
		}
		
		if HP <= 0 {
			self.dead = true
			self.physicsBody?.velocity = CGVectorMake(0.0, 0.0)
			self.physicsBody?.angularVelocity = 0.0
			particleFireSaw2!.hidden = false
			self.particleFireSaw2!.position = self.position
			self.particleFireSaw2!.particleSpeed = 10.0
			self.particleSmoke!.hidden = true
		}
		
		if !dead {
			if rotationDirection != 0 {
				self.physicsBody?.angularVelocity = 4.0 * CGFloat(rotationDirection)
				
			} else {
				if self.physicsBody?.angularVelocity > 0 {
					self.physicsBody?.angularVelocity -= 0.5
					
					if self.physicsBody?.angularVelocity < 0 {
						self.physicsBody?.angularVelocity = 0
					}
				} else if self.physicsBody?.angularVelocity < 0 {
					self.physicsBody?.angularVelocity += 0.5
					
					if self.physicsBody?.angularVelocity > 0 {
						self.physicsBody?.angularVelocity = 0
					}
				} else {
					self.physicsBody?.angularVelocity = 0
				}
			}
		
			self.physicsBody?.velocity = CGVectorMake(-sin(self.zRotation) * carVelocity, cos(self.zRotation) * carVelocity)
			
		}
		
		weapon.update(self, scene: scene, currentTime: currentTime)
		
		if weapon.weaponDuration <= 0 {
			weaponActivated = false
		}
		
		if weapon.defenseDuration <= 0 {
			defenseActivated = false
			self.particleFireSaw1!.removeFromParent()
			weapon.defense1.removeFromParent()
		}
		
		if timeNextCollision > 0 {
			timeNextCollision -= deltaTime
		}
		
		if timeNextSawCollision > 0 {
			timeNextSawCollision -= deltaTime
		}
	}
}
